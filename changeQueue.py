#!/usr/bin/python
from mininet.net import Mininet
from mininet.node import OVSSwitch, RemoteController
from topos import RingTopo
import sys
import urllib
import json
import requests
import time
import subprocess
from qos import setup_qos, clear_qos

PORT = 8888
URL = 'http://localhost:8080/tee/routes/10.0.0.1/10.0.0.3'
URL_MOVE_F = 'http://localhost:8080/tee/move/{fromRoute}/{flow}/{toRoute}'
URL_QUEUE_F = 'http://localhost:8080/tee/changeQueue/{flow}/{queue}'

# Test constants
SWITCH_COUNT = 4
HOST_PER_SWITCH = 1
BANDWIDTH = 10


def command_wrapper(h, cmd):
    sys.stdout.write("{host} :: {cmd} ... ".format(host=h, cmd=cmd))
    sys.stdout.flush()
    h.cmd(cmd)
    sys.stdout.write("Done\n")


def main():
    net = Mininet(topo=RingTopo(k=SWITCH_COUNT, n=HOST_PER_SWITCH), controller=RemoteController, switch=OVSSwitch,
                  autoSetMacs=True)
    print "Starting network.."
    print "Number of switches: {}".format(SWITCH_COUNT)
    print "Hosts per switch: {}".format(HOST_PER_SWITCH)
    print "Link bandwidth: {} Mb/s".format(BANDWIDTH)
    net.start()
    setup_qos()

    g_passed = True
    queues = subprocess.check_output("sudo ovs-ofctl queue-stats s1".split()).split('\n')
    if len(queues[1:-1]) != 9:
        print "ERROR Wrong number ({}) of queues on switch s1. Expected (9)".format(len(queues[1:-1]))
        print str(queues[1:-1])
        g_passed = False

    print "Hosts:"
    print net.hosts

    h1 = net.getNodeByName("h1")
    h3 = net.getNodeByName("h3")

    command_wrapper(h3, 'iperf -s &')
    time.sleep(10)
    command_wrapper(h1, 'iperf -t 10000 -c 10.0.0.3&')

    while True:
        try:
            text = urllib.urlopen(URL).read()
            js = json.loads(text)
            if js:
                if len(js) < 2:
                    continue
                r1 = js[0]
                r2 = js[1]
                break
            time.sleep(1)
        except (ValueError, IOError):
            time.sleep(1)
        print "Retrying to read from REST API.."

    if not js[0]['flows']:
        r1, r2 = r2, r1

    r1_id = r1['id']
    r2_id = r2['id']

    print ""
    print "Press enter to change flows queue to 1 ..."
    sys.stdin.read(1)
    for f in r1['flows']:
        url = URL_QUEUE_F.format(flow=f['id'], queue=1)
        r = requests.get(url)
        print "HTTP GET :: " + url
        print "Response " + str(r)

    print ""
    print "Press enter to move flows to other route..."
    sys.stdin.read(1)
    for f in r1['flows']:
        url = URL_MOVE_F.format(fromRoute=r1_id, flow=f['id'], toRoute=r2_id)
        print "Move flow id:"+str(f['id'])+" ("+f['srcIP']+" -> "+f['dstIP']+")"
        r = requests.get(url)
        print "HTTP GET :: " + url
        print "Response " + str(r)

        # Verify if flows were moved
        passed = True
        text = urllib.urlopen(URL).read()
        js = json.loads(text)
        if js:
            r1n = js[0]
            r2n = js[1]
        if r1n['id'] != r1_id:
            r1n, r2n = r2n, r1n
        # Check if flow was removed from old route
        if len([flow['id'] for flow in r1n['flows'] if flow['id'] == f['id']]) != 0:
            print "Error: flow {} was not removed from old route {}".format(f['id'], [flow['id'] for flow in r1n['flows']])
            passed = False
        # Check if flow was added to new route
        if len([flow for flow in r2n['flows'] if flow['id'] == f['id']]) != 1:
            print "Error: flow was not added to new route"
            passed = False
        if not passed:
            g_passed = False

    print ""
    print "Press enter to change flows queue to 2 ..."
    sys.stdin.read(1)
    for f in r1['flows']:
        url = URL_QUEUE_F.format(flow=f['id'], queue=2)
        r = requests.get(url)
        print "HTTP GET :: " + url
        print "Response " + str(r)

    print ""
    print "Press enter to change flows queue to 0 (default) ..."
    sys.stdin.read(1)
    for f in r1['flows']:
        url = URL_QUEUE_F.format(flow=f['id'], queue=0)
        r = requests.get(url)
        print "HTTP GET :: " + url
        print "Response " + str(r)

    if g_passed:
        print "Test PASSED"
    else:
        print "Test FAILED"

    print ""
    print "Press enter to close ..."
    sys.stdin.read(1)
    print "Closing network"
    net.stop()
    clear_qos()


if __name__ == "__main__":
    main()
