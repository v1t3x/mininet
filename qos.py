#!/usr/bin/python
from subprocess import *
import sys

CMD_LIST_BRIDGES = "sudo ovs-vsctl --real list-br"
CMD_LIST_PORTS = "sudo ovs-vsctl list-ports {bridge}"
CMD_CREATE_QUEUES = "sudo ovs-vsctl -- --id=@q0 create Queue other-config:max-rate=10000000 -- --id=@q1 create Queue other-config:max-rate=5000000 -- --id=@q2 create Queue other-config:max-rate=2500000"
CMD_CREATE_QOS = "sudo ovs-vsctl -- --id=@newqos create QoS type=linux-htb other-config:max-rate=10000000 queues=0={q0},1={q1},2={q2}"
CMD_ASSIGN_QOS = "sudo ovs-vsctl -- set Port {port} qos={qos}"
CMD_CLEAR_QUEUES = "sudo sudo ovs-vsctl --all destroy Queue"
CMD_CLEAR_QOS = "sudo ovs-vsctl --all destroy QoS"


def get_ports(bridge):
    return check_output(CMD_LIST_PORTS.format(bridge=bridge).split()).split()


def get_bridges():
    return check_output(CMD_LIST_BRIDGES.split()).split()


def create_queues():
    return check_output(CMD_CREATE_QUEUES.split()).split()


def create_qos(queues):
    return check_output(CMD_CREATE_QOS.format(q0=queues[0], q1=queues[1], q2=queues[2]).split()).strip()


def assign_qos(port, qos):
    return check_output(CMD_ASSIGN_QOS.format(port=port, qos=qos).split())


def clear_qos():
    print "=== Clearing QoS ==="
    print "QoS::Removing all QoS"
    call(CMD_CLEAR_QOS.split())
    print "QoS::Removing all queues"
    call(CMD_CLEAR_QUEUES.split())
    print "===  QoS clear   ==="


def setup_qos():
    print "=== Setting QoS ==="
    print "= WARNING do not set bandwidth limit by TCLink in order QoS to work ="
    queues = create_queues()
    print "QoS::Created queues: {}".format(queues)
    qos = create_qos(queues)
    print "QoS::Created QoS: {}".format(qos)
    bridges = get_bridges()
    for bridge in bridges:
        print "QoS::Configuring {}:".format(bridge)
        ports = get_ports(bridge)
        for port in ports:
            assign_qos(port, qos)
            print "QoS:: = Port {} : QoS = {}".format(port, qos)
    print "===   QoS Set   ==="


if __name__ == "__main__":
    if len(sys.argv) > 1:
        clear_qos()
    else:
        setup_qos()